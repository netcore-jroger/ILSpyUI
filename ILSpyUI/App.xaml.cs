﻿using System.Windows;
using TomsToolbox.Wpf.Styles;

namespace ILSpyUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // this.Resources.MergedDictionaries.Insert(0, WpfStyles.GetDefaultStyles().RegisterDefaultWindowStyle());
            this.Resources.RegisterDefaultStyles();
            ThemeManager.Current.IsDarkMode = false;
        }
    }
}
