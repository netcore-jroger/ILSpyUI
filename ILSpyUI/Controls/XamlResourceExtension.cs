﻿using System;
using System.Windows.Markup;

namespace ILSpyUI.Controls
{
    class XamlResourceExtension : MarkupExtension
    {
        private readonly string name;

        public XamlResourceExtension(string name)
        {
            this.name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Images.Load(null, name);
        }
    }
}
