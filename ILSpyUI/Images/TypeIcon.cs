﻿namespace ILSpyUI
{
    internal enum TypeIcon
    {
        Class,
        Enum,
        Struct,
        Interface,
        Delegate
    }
}
