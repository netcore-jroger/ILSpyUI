﻿namespace ILSpyUI
{
    internal enum AccessOverlayIcon
    {
        Public,
        Protected,
        Internal,
        ProtectedInternal,
        Private,
        PrivateProtected,
        CompilerControlled
    }
}
