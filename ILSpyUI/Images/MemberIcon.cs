﻿namespace ILSpyUI
{
    internal enum MemberIcon
    {
        Literal,
        FieldReadOnly,
        Field,
        EnumValue,
        Property,
        Indexer,
        Method,
        Constructor,
        VirtualMethod,
        Operator,
        ExtensionMethod,
        PInvokeMethod,
        Event
    }
}
